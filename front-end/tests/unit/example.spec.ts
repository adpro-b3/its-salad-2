import { shallowMount } from "@vue/test-utils";
import GameOver from "@/views/GameOver.vue";
import Player from '@/models/player';

describe("GameOver.vue", () => {
  it("renders without error with score equals to zero", () => {
    const msg = "new message";
    const wrapper = shallowMount(GameOver, {
      propsData: {
        player: new Player(),
      }
    });
    expect(wrapper.text()).toContain(`Score: 0`);
  });
});
