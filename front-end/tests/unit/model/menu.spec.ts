import { shallowMount } from "@vue/test-utils";
import Menu from "@/models/menu/menu";

describe("test Menu generate the New Menu", () => {
    it("add some condiments to the menu based on the level", () => {
        const menu: Menu = new Menu();
        menu.generateMenu("easy");

        const easyMenu = menu.getMenu();
        expect(easyMenu.get("Apple")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Apple")).toBeLessThan(4);
        expect(easyMenu.get("Banana")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Banana")).toBeLessThan(4);
        expect(easyMenu.get("Grape")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Grape")).toBeLessThan(4);
        expect(easyMenu.get("Lettuce")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Lettuce")).toBeLessThan(4);
        expect(easyMenu.get("Corn")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Corn")).toBeLessThan(4);
        expect(easyMenu.get("Tomato")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Tomato")).toBeLessThan(4);
        expect(easyMenu.get("Mayonnaise")).toBeGreaterThanOrEqual(0);
        expect(easyMenu.get("Mayonnaise")).toBeLessThan(4);

        menu.generateMenu("medium");
        const mediumMenu = menu.getMenu();
        expect(mediumMenu.get("Apple")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Apple")).toBeLessThan(6);
        expect(mediumMenu.get("Banana")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Banana")).toBeLessThan(6);
        expect(mediumMenu.get("Grape")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Grape")).toBeLessThan(6);
        expect(mediumMenu.get("Lettuce")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Lettuce")).toBeLessThan(6);
        expect(mediumMenu.get("Corn")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Corn")).toBeLessThan(6);
        expect(mediumMenu.get("Tomato")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Tomato")).toBeLessThan(6);
        expect(mediumMenu.get("Mayonnaise")).toBeGreaterThanOrEqual(0);
        expect(mediumMenu.get("Mayonnaise")).toBeLessThan(6);

        menu.generateMenu("hard");
        const hardMenu = menu.getMenu();
        expect(hardMenu.get("Apple")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Apple")).toBeLessThan(7);
        expect(hardMenu.get("Banana")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Banana")).toBeLessThan(7);
        expect(hardMenu.get("Grape")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Grape")).toBeLessThan(7);
        expect(hardMenu.get("Lettuce")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Lettuce")).toBeLessThan(7);
        expect(hardMenu.get("Corn")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Corn")).toBeLessThan(7);
        expect(hardMenu.get("Tomato")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Tomato")).toBeLessThan(7);
        expect(hardMenu.get("Mayonnaise")).toBeGreaterThanOrEqual(1);
        expect(hardMenu.get("Mayonnaise")).toBeLessThan(7);
    });
});