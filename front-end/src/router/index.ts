import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

import Landing from "../views/landing/Landing.vue";
import Ready from "../views/ready/Ready.vue";
import Instructions from "../views/instructions/Instructions.vue";


Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Landing",
    component: Landing
  },
  {
    path: "/ready",
    name: "Ready",
    component: Ready
  },
  {
    path: "/how-to-play",
    name: "Instructions",
    component: Instructions
  },
  {
    path: "/game",
    name: "Game",
    component: () => import("../views/game/Game.vue")
  },
  {
    path: "/leaderboard",
    name: "leaderboard",
    component: () => import("../views/LeaderBoard.vue")
  },
  {
    path: "/game-over",
    name: "GameOver",
    component: () => import("../views/GameOver.vue"),
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
