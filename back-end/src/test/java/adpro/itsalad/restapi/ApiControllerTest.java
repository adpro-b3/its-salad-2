//package adpro3.itsalad.RestApi;
//
//import adpro3.itsalad.RestApi.controller.ApiController;
//import adpro3.itsalad.RestApi.repository.PlayerRepository;
//import adpro3.itsalad.gamepage.service.DoneService;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
//import static org.hamcrest.Matchers.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@ExtendWith(SpringExtension.class)
//@WebMvcTest(controllers = ApiController.class)
//@AutoConfigureMockMvc
//public class ApiControllerTest {
//
//  @Autowired
//  private MockMvc mockMvc;
//
//  @MockBean
//  private DoneService doneService;
//
//  @MockBean
//  private PlayerRepository playerRepository;
//
//  @Test
//  public void scoreUrlIsTrue() throws Exception{
//    mockMvc.perform(post("/json-score")
//            .contentType(MediaType.APPLICATION_JSON)
//            .header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE"))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$.*", hasSize(3)))
//            .andExpect(jsonPath("$.score", is(0)))
//            .andExpect(jsonPath("$.bestScore", is(0)));
//  }
//
//  @Test
//  public void bestScoreUrlIsTrue() throws Exception{
//    mockMvc.perform(post("/json-best-score")
//            .contentType(MediaType.APPLICATION_JSON)
//            .header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE"))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$.*", hasSize(3)))
//            .andExpect(jsonPath("$.score", is(0)))
//            .andExpect(jsonPath("$.bestScore", is(0)));
//  }
//
//  @Test
//  public void resetScoreUrlIsTrue() throws Exception{
//    mockMvc.perform(post("/reset-score")
//            .contentType(MediaType.APPLICATION_JSON)
//            .header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE"))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$.*", hasSize(2)))
//            .andExpect(jsonPath("$.score", is(0)))
//            .andExpect(jsonPath("$.bestScore", is(0)));
//  }
//
//  @Test
//  public void resetAllUrlIsTrue() throws Exception{
//    mockMvc.perform(post("/reset-all-score")
//            .contentType(MediaType.APPLICATION_JSON)
//            .header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE"))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$.*", hasSize(3)))
//            .andExpect(jsonPath("$.score", is(0)))
//            .andExpect(jsonPath("$.bestScore", is(0)));
//  }
//
//
//}
