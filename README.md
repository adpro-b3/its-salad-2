# It's Salad

[![pipeline status](https://gitlab.com/adpro-b3/its-salad-2/badges/master/pipeline.svg)](https://gitlab.com/adpro-b3/its-salad-2/-/commits/master)

## Test Coverage Status
- [![Back-End Service Coverage Report](https://gitlab.com/adpro-b3/its-salad-2/badges/master/coverage.svg?job=BackEnd-Test)](https://gitlab.com/adpro-b3/its-salad-2/-/commits/master) Back-End Coverage Report (Spring, Java)
- [![Front-End Service Coverage Report](https://gitlab.com/adpro-b3/its-salad-2/badges/master/coverage.svg?job=FrontEnd-Test)](https://gitlab.com/adpro-b3/its-salad-2/-/commits/master) Front-End Coverage Report (Vue, JavaScript)

It’s SALAD! adalah sebuah aplikasi berbasis website yang dapat digunakan oleh
Client untuk menerima request dari customer (auto-generated dari program)
berupa permintaan menu Salad.

## Our Website
https://adpro-b3.herokuapp.com/

### Pengembang
Terdiri dari mahasiswa/i yang tergabung dalam kelompok 3 mata kuliah _Advance Programming_ kelas B
1. Muzakki Hassan
2. Idris Yoga 
3. Eugene Brigita
4. Ilma Ainur Rohma
5. Louisa Natalika
