package adpro.itsalad.score.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreTest {
  Score score;

  @BeforeEach
  public void setUp() {
    score = new Score();
  }

  @Test
  public void checkUpdate() {
    int temp = score.getScore();
    score.updateScore();
    assertEquals(++temp, score.getScore());
  }

    @Test
    public void checkCondimentsTest(){
        int temp = score.getScore();
        score.updateScore();
        assertEquals(++temp, score.getScore());
    }

    @Test
    public void checkResetScore(){
        score.resetScore();
        assertEquals(0, score.getScore());
    }
}
