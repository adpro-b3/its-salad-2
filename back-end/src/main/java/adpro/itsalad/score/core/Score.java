package adpro.itsalad.score.core;

public class Score {
  private int score;

  public Score() {
    this.score = 0;
  }

  public int updateScore() {
    return score++;
  }

  public int getScore() {
    return score;
  }

  public void resetScore() {
    this.score = 0;
  }

}


