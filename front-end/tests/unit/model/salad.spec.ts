import { shallowMount } from "@vue/test-utils";
import GameOver from "@/views/GameOver.vue";
import Player from "@/models/player";
import { SaladBuilder, ConcreteSaladBuilder, Apple, Banana, Corn, Grape, Lettuce, Mayonnaise, Tomato } from "@/models/salad/salad";

describe("tests SaladBuilder produces correct Salad", () => {
  it("produces correct condiments with correct quantity given set of operations", () => {
    const saladBuilder: SaladBuilder = new ConcreteSaladBuilder();
    saladBuilder
      .addApple()
      .addBanana()
      .addCorn()
      .addGrape()
      .addLettuce()
      .addMayonnaise()
      .addTomato();

    const producedSalad = saladBuilder.getSalad();
    expect(producedSalad.condiments[0]).toBeInstanceOf(Apple);
    expect(producedSalad.condiments[1]).toBeInstanceOf(Banana);
    expect(producedSalad.condiments[2]).toBeInstanceOf(Corn);
    expect(producedSalad.condiments[3]).toBeInstanceOf(Grape);
    expect(producedSalad.condiments[4]).toBeInstanceOf(Lettuce);
    expect(producedSalad.condiments[5]).toBeInstanceOf(Mayonnaise);
    expect(producedSalad.condiments[6]).toBeInstanceOf(Tomato);
    expect(producedSalad.condimentsTotal).toStrictEqual([1, 1, 1, 1, 1, 1, 1]);
  });

  it("resets builder to initial state when reset() is called", () => {
    const saladBuilder: SaladBuilder = new ConcreteSaladBuilder().addApple().addBanana();
    const currentSalad = saladBuilder.getSalad();
    saladBuilder.reset();
    
    const resettedSalad = saladBuilder.getSalad();
    expect(resettedSalad.condiments).toStrictEqual([]);    
    expect(resettedSalad.condimentsTotal).toStrictEqual([0, 0, 0, 0, 0, 0, 0]);

    expect(currentSalad).not.toBe(resettedSalad);
  });
});
