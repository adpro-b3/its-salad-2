import { shallowMount } from "@vue/test-utils";
import Player from "@/models/player";

describe("Test Player", () => {
    it("Player has default attribute", () => {
        const player: Player = new Player();

        expect(player.name).toEqual("");
        expect(player.score).toEqual(0);
        expect(player.remainingTime).toEqual(100);
        expect(player.bestScore).toEqual(0);
        expect(player.level).toEqual("easy");
    });

    it("Player's level will increase", () => {
        // default level of player
        const player: Player = new Player();
        expect(player.level).toEqual("easy");

        //Increase player's level
        player.nextLevel();
        expect(player.level).toEqual("medium");

        //Increase player's level one more time
        player.nextLevel();
        expect(player.level).toEqual("hard");
    });

    it("Player's name can be changed", () => {
        //default player's name
        const player: Player = new Player();
        expect(player.name).toEqual("");

        //Change the player's name
        player.setName("pemain");
        expect(player.name).toEqual("pemain");
    });

    it("Player's score and time will be reseted", () => {
        //default player's name
        const player: Player = new Player();

        //Change the player score and time
        player.score = 10;
        player.remainingTime = 50;
        expect(player.score).toEqual(10);
        expect(player.remainingTime).toEqual(50);

        //Reset the player
        player.reset();

        expect(player.score).toEqual(0);
        expect(player.remainingTime).toEqual(100);
    });
});