import globalState, {player} from "@/models/globalState";
import router from "@/router";
import axios from "axios";
export default {
  methods: {
    navigateToInstructions(){
      router.push("/how-to-play");
    },
    initGame() {
      axios.get("https://adpro-b3-api.herokuapp.com/initGame").then(response => player.playId = response.data.playId);
    }
  },
  data: function() {
    return {
      player: globalState.player,
    };
  }
};
