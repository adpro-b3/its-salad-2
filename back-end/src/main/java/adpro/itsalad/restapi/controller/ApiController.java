package adpro.itsalad.restapi.controller;

import adpro.itsalad.restapi.jsonview.Views;
import adpro.itsalad.restapi.model.AjaxResponseBody;
import adpro.itsalad.restapi.model.Player;
import adpro.itsalad.restapi.repository.PlayerRepository;
import adpro.itsalad.score.service.ScoreService;
import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
  @Autowired
  ScoreService scoreService;

  @Autowired
  PlayerRepository playerRepository;

  @JsonView(Views.Public.class)
  @RequestMapping(value = "/initGame")
  public AjaxResponseBody initGame() {
    AjaxResponseBody res = new AjaxResponseBody();
    scoreService.setPlayId();
    res.setPlayId(scoreService.getPlayId());
    return res;
  }

  @JsonView(Views.Public.class)
  @RequestMapping(value = "/json-score")
  public AjaxResponseBody getJsonScore() {
    AjaxResponseBody res = new AjaxResponseBody();
    scoreService.done();
    res.setScore(scoreService.getScore());
    res.setPlayId(scoreService.getPlayId());
    return res;
  }

  @JsonView(Views.Public.class)
  @RequestMapping(value = "/json-best-score")
  public AjaxResponseBody getJsonBestScore() {
    AjaxResponseBody res = new AjaxResponseBody();
    scoreService.checkBestScore(scoreService.getScore());
    res.setScore(scoreService.getScore());
    res.setBestScore(scoreService.getBestScore());
    res.setPlayId(scoreService.getPlayId());
    return res;
  }

  @JsonView(Views.Public.class)
  @RequestMapping(value = "/reset-score")
  public AjaxResponseBody getResetScore() {
    AjaxResponseBody res = new AjaxResponseBody();
    scoreService.resetScore();
    res.setScore(scoreService.getScore());
    res.setBestScore(scoreService.getBestScore());
    res.setPlayId(scoreService.getPlayId());
    return res;
  }

  @JsonView(Views.Public.class)
  @RequestMapping(value = "/reset-all-score")
  public AjaxResponseBody getResetAll() {
    addPlayer();
    AjaxResponseBody res = new AjaxResponseBody();
    scoreService.resetAll();
    res.setScore(scoreService.getScore());
    res.setBestScore(scoreService.getBestScore());
    res.setPlayId(scoreService.getPlayId());
    return res;
  }

  @GetMapping("/get-all-player")
  public List<Player> getAllPlayer() {
    return playerRepository.findAll(Sort.by(Sort.Direction.DESC, "bestScore"));
  }

  public Player addPlayer() {
    Player player = new Player();
    player.setPlayId(scoreService.getPlayId());
    player.setBestScore(scoreService.getBestScore());
    return playerRepository.save(player);
  }
}
