package adpro.itsalad.score.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreServiceTest {
  ScoreService scoreService;

  @BeforeEach
  public void setUp() {
    scoreService = new ScoreService();
  }

    @Test
    public void checkDone(){
        int temp = scoreService.getScore();
        scoreService.done();
        assertEquals(++temp, scoreService.getScore());
    }

    @Test
    public void checkReset(){
        scoreService.resetScore();
        assertEquals(0, scoreService.getScore());
    }

    @Test
    public void checkReseAll(){
      scoreService.resetAll();
      assertEquals(0, scoreService.getScore());
      assertEquals(0, scoreService.getBestScore());
    }

    @Test
    public void checkBestScore(){
        scoreService.checkBestScore(5);
        assertEquals(5, scoreService.getBestScore());
        int bestScore = scoreService.getBestScore();
        scoreService.checkBestScore(0);
        assertEquals(bestScore, scoreService.getBestScore());
    }

    @Test
    public void testGenerateId() {
      scoreService.setPlayId();
      assertEquals(scoreService.getPlayId().length(), 5);
    }



}
