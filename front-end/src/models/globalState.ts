import Player from "./player";
import { SaladBuilder, ConcreteSaladBuilder } from "./salad/salad";
import Menu from "./menu/menu";

export const player = new Player();

export const saladBuilder = new ConcreteSaladBuilder();

export const menu = new Menu();

export interface GlobalState {
  player: Player;
  saladBuilder: SaladBuilder;
  menu: Menu;
}

const globalState: GlobalState = {
  player,
  saladBuilder,
  menu
};

export default globalState;
