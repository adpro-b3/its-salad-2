import globalState from "@/models/globalState";
import router from "@/router";

export default {
  methods: {
    navigateToReady() {
      router.push("/ready");
    },
    navigateToInstructions(){
      router.push("/how-to-play");
    },
    navigateToLeaderboard(){
      router.push("/leaderboard");
    }
  },
  data: function() {
    return {
      ...globalState
    };
  }
};
