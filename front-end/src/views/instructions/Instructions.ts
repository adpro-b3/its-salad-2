import globalState from "@/models/globalState";
import router from "@/router";


export default {
  methods: {
    navigateToLanding(){
      router.go(-1);
    },
  },
  data: function() {
    return {
      player: globalState.player,
    };
  }
};
