import globalState from "@/models/globalState";
import axios from "axios";
import { player } from "@/models/globalState";
import { saladBuilder } from "@/models/globalState";

function checkApple() {
    if(globalState.menu.getMenu().get("Apple") === saladBuilder.getSalad().condimentsTotal[0]){
        return true;
    }
    else{
        return false;
    }
}

function checkBanana() {
    if(globalState.menu.getMenu().get("Banana") === saladBuilder.getSalad().condimentsTotal[1]){
        return true;
    }
    else{
        return false;
    }
}

function checkGrape() {
    if(globalState.menu.getMenu().get("Grape") === saladBuilder.getSalad().condimentsTotal[2]){
        return true;
    }
    else{
        return false;
    }
}

function checkLettuce() {
    if(globalState.menu.getMenu().get("Lettuce") === saladBuilder.getSalad().condimentsTotal[3]){
        return true;
    }
    else{
        return false;
    }
}

function checkCorn() {
    if(globalState.menu.getMenu().get("Corn") === saladBuilder.getSalad().condimentsTotal[4]){
        return true;
    }
    else{
        return false;
    }
}

function checkTomato() {
    if(globalState.menu.getMenu().get("Tomato") === saladBuilder.getSalad().condimentsTotal[5]){
        return true;
    }
    else{
        return false;
    }
}

function checkMayonnaise() {
    if(globalState.menu.getMenu().get("Mayonnaise") === saladBuilder.getSalad().condimentsTotal[6]){
        return true;
    }
    else{
        return false;
    }
}


function checkCondiment() {
    if(checkApple() && checkBanana() && checkGrape() && checkLettuce() && checkCorn() && checkTomato() && checkMayonnaise()){
        return true;
    }
    else{
        return false;
    }

}

export default {
    data: function() {
        return {
            salad: globalState.saladBuilder.getSalad(),
            player: player,
        }
    },
    methods : {
        buttonDone(){
            if(checkCondiment()) {
                axios.get("https://adpro-b3-api.herokuapp.com/json-score")
                    .then(response => player.score =  response.data.score);
                if(player.score == 6 ){
                    player.nextLevel();
                    alert('Next Level! You\'re on level ' + player.level);
                } else if (player.score == 4){
                    player.nextLevel();
                    alert('Next Level! You\'re on level ' + player.level);
                }
                globalState.menu.generateMenu(player.level);
                globalState.saladBuilder.reset();
            }
            else{
                alert("Ooops! It's like you're missing something. Please check again your condiments against the menu")
            }
        },
        buttonReset(){
            globalState.saladBuilder.reset();
        }
    }
}
