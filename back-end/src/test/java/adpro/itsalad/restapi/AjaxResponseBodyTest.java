package adpro.itsalad.restapi;

import adpro.itsalad.restapi.jsonview.Views;
import adpro.itsalad.restapi.model.AjaxResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AjaxResponseBodyTest {
  AjaxResponseBody ajaxResponseBody;

  @BeforeEach
  public void setUp(){
    ajaxResponseBody = new AjaxResponseBody();
  }

  @Test
  public void jsonViewTest() throws JsonProcessingException {
    ajaxResponseBody.setScore(2);
    ajaxResponseBody.setBestScore(5);

    assertEquals(2, ajaxResponseBody.getScore());
    assertEquals(5, ajaxResponseBody.getBestScore());

    ObjectMapper mapper = new ObjectMapper();
    String result = mapper
            .writerWithView(Views.Public.class)
            .writeValueAsString(ajaxResponseBody);

    assertThat(result, containsString("2"));
    assertThat(result, containsString("5"));
  }
}
