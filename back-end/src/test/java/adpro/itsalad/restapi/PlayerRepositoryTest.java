package adpro.itsalad.restapi;

import adpro.itsalad.restapi.model.Player;
import adpro.itsalad.restapi.repository.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
public class PlayerRepositoryTest {
  @Autowired
  private PlayerRepository playerRepository;

  @Test
  public void playerTest() {
    Player player = new Player();
    player.setBestScore(7);
    player.setPlayId("ABCDE");
    assertNotNull(player);
  }

  @Test
  public void savePlayerTest() {
    Player player = new Player();
    player.setBestScore(7);
    player.setPlayId("ABCDE");
    playerRepository.save(player);
    assertNotNull(playerRepository.findAll());
  }

  @Test
  public void savedPlayerIsTrue() {
    Player player = new Player();
    player.setBestScore(7);
    player.setPlayId("ABCDE");
    assertNotNull(player);
    assertEquals(player.getBestScore(), 7);
    assertEquals(player.getPlayId(), "ABCDE");
  }
}
