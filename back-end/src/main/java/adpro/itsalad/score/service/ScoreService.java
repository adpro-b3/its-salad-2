package adpro.itsalad.score.service;

import adpro.itsalad.score.core.Score;
import org.springframework.stereotype.Service;

@Service
public class ScoreService {
  Score score;
  int bestScore = 0;
  String playId;
  private final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  public ScoreService() {
    this.score = new Score();
  }

  /*
  * will execute when done button pressed
   */
  public void done() {
    this.score.updateScore();
  }

  public int getScore() {
    return this.score.getScore();
  }

  public void resetScore() {
    this.score.resetScore();
  }

  public void resetAll() {
    this.score.resetScore();
    this.bestScore = 0;
    this.playId = "";
  }

  public String generatePlayId() {
    int count = 5;
    StringBuilder builder = new StringBuilder();
    while (count-- != 0) {
      int character = (int)(Math.random() * alphabet.length());
      builder.append(alphabet.charAt(character));
    }
    return builder.toString();
  }

  public void setPlayId() {
    this.playId = generatePlayId();
  }

  public String getPlayId() {
    return playId;
  }

  /*
  *
   * evaluate the best score
   * @param newScore
   */
  public void checkBestScore(int newScore) {
    if (newScore >= bestScore) {
      this.bestScore = newScore;
    }
  }

  public int getBestScore() {
    return this.bestScore;
  }
}
