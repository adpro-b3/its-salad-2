import { player } from "@/models/globalState";
import router from '@/router';

export default {
  data() {
    return {
      player: player
    };
  },
  created: function() {
    const updateTimer = setInterval(function() {
      if (player.remainingTime <= 0) {
          clearInterval(updateTimer);
          router.push('/game-over');
      } else {
          player.remainingTime -= 1;
      }
    }, 1000);
  }
};
