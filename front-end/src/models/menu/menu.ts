export class Menu{
    menus = new Map();

  
    constructor() {
      this.menus.set('Apple', 0);
      this.menus.set('Banana', 0);
      this.menus.set('Grape', 0);
      this.menus.set('Lettuce', 0);
      this.menus.set('Corn', 0);
      this.menus.set('Tomato', 0);
      this.menus.set('Mayonnaise', 0);
    }

    public generateMenu(level: string):void{
        if(level=="easy"){
            this.menus.set('Apple', Math.floor(Math.random()*3));
            this.menus.set('Banana', Math.floor(Math.random()*3));
            this.menus.set('Grape', Math.floor(Math.random()*3));
            this.menus.set('Lettuce', Math.floor(Math.random()*3));
            this.menus.set('Corn', Math.floor(Math.random()*3));
            this.menus.set('Tomato', Math.floor(Math.random()*3));
            this.menus.set('Mayonnaise', Math.floor(Math.random()*3));
        } else if (level == "medium"){
            this.menus.set('Apple', Math.floor(Math.random()*5));
            this.menus.set('Banana', Math.floor(Math.random()*5));
            this.menus.set('Grape', Math.floor(Math.random()*5));
            this.menus.set('Lettuce', Math.floor(Math.random()*5));
            this.menus.set('Corn', Math.floor(Math.random()*5));
            this.menus.set('Tomato', Math.floor(Math.random()*5));
            this.menus.set('Mayonnaise', Math.floor(Math.random()*5));
        } else if (level == "hard"){
            this.menus.set('Apple', Math.floor(Math.random()*6) + 1);
            this.menus.set('Banana', Math.floor(Math.random()*6) + 1);
            this.menus.set('Grape', Math.floor(Math.random()*6) + 1);
            this.menus.set('Lettuce', Math.floor(Math.random()*6) + 1);
            this.menus.set('Corn', Math.floor(Math.random()*6) + 1);
            this.menus.set('Tomato', Math.floor(Math.random()*6) + 1);
            this.menus.set('Mayonnaise', Math.floor(Math.random()*6) + 1);
        }
        
    }

    public getMenu():Map<string,number> {
        console.log(this.menus);
        return this.menus;
    }
 

}

export default Menu;