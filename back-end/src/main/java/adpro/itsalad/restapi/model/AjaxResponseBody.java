package adpro.itsalad.restapi.model;

import adpro.itsalad.restapi.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

public class AjaxResponseBody {
  @JsonView(Views.Public.class)
  int score;

  @JsonView(Views.Public.class)
  int bestScore;

  @JsonView(Views.Public.class)
  String playId;

  public int getScore() {
    return score;
  }

  public int getBestScore() {
    return bestScore;
  }

  public String getPlayId() {
    return playId;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public void setBestScore(int bestScore) {
    this.bestScore = bestScore;
  }

  public void setPlayId(String playId) {
    this.playId = playId;
  }
}
