class Player {
  name: string;
  score: number;
  remainingTime: number;
  bestScore: number;
  level: string;
  playId: string;

  
  constructor(name = "", score = 0, remainingTime = 100) {
    this.name = name;
    this.score = score;
    this.remainingTime = remainingTime;
    this.bestScore = 0;
    this.level = "easy";
    this.playId = "";
  }


  nextLevel(){
    if(this.level == "easy"){
      this.level = "medium";
    } else {
      this.level = "hard";
    }
  }

  setName(newName: string) {
    this.name = newName;
  }

  reset() {
    this.score = 0;
    this.remainingTime = 100;
  }
}

export default Player;
