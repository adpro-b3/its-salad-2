import GameCanvas from "@/components/GameCanvas/GameCanvas.vue";
import Menu from "@/components/Menu/Menu.vue";
import LeftMenu from "@/components/LeftMenu/LeftMenu.vue";

export default {
  components: {
    LeftMenu,
    GameCanvas,
    Menu
  },
};
