import globalState from "@/models/globalState";

const builder = globalState.saladBuilder;

export default {
    methods: {
        addApple: builder.addApple,
        addBanana: builder.addBanana,
        addCorn: builder.addCorn,
        addGrape: builder.addGrape,
        addLettuce: builder.addLettuce,
        addMayonnaise: builder.addMayonnaise,
        addTomato: builder.addTomato,
    },
    data: function() {
        return {
            salad: builder.getSalad(),
        };
    }
};
