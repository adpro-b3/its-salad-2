import globalState from "@/models/globalState";
import { player } from "@/models/globalState";

globalState.menu.generateMenu(player.level);

export default {
    data: function() {
        return {
            menu: globalState.menu.getMenu(),
            player : player
        }
    }
}