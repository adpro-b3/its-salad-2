interface Condiment {
  name: string;
}

export class Apple implements Condiment {
  name = "Apple";
}

export class Banana implements Condiment {
  name = "Banana";
}

export class Corn implements Condiment {
  name = "Corn";
}

export class Grape implements Condiment {
  name = "Grape";
}

export class Lettuce implements Condiment {
  name = "Lettuce";
}

export class Mayonnaise implements Condiment {
  name = "Mayonaise";
}

export class Tomato implements Condiment {
  name = "Tomato";
}

export class Salad {
  condiments: Condiment[];
  condimentsTotal: number[];

  constructor() {
    this.condiments = [];
    this.condimentsTotal = [0,0,0,0,0,0,0];
  }
  
}

export interface SaladBuilder {
  addApple(): SaladBuilder;
  addBanana(): SaladBuilder;
  addCorn(): SaladBuilder;
  addGrape(): SaladBuilder;
  addLettuce(): SaladBuilder;
  addMayonnaise(): SaladBuilder;
  addTomato(): void;
  getSalad(): Salad;
  reset(): void;
}

export class ConcreteSaladBuilder implements SaladBuilder {
  private salad: Salad;

  constructor() {
    this.salad = new Salad();
  }

  public reset(): void {
    this.salad = new Salad();
  }

  addApple(): SaladBuilder {
    this.salad.condiments.push(new Apple());
    this.salad.condimentsTotal[0]++;
    return this;
  }
  addBanana(): SaladBuilder {
    this.salad.condiments.push(new Banana());
    this.salad.condimentsTotal[1]++;
    return this;
  }
  addGrape(): SaladBuilder {
    this.salad.condiments.push(new Grape());
    this.salad.condimentsTotal[2]++;
    return this;
  }
  addLettuce(): SaladBuilder {
    this.salad.condiments.push(new Lettuce());
    this.salad.condimentsTotal[3]++;
    return this;
  }
  addCorn(): SaladBuilder {
    this.salad.condiments.push(new Corn());
    this.salad.condimentsTotal[4]++;
    return this;
  }
  addTomato(): SaladBuilder {
    this.salad.condiments.push(new Tomato());
    this.salad.condimentsTotal[5]++;
    return this;
  }
  addMayonnaise(): SaladBuilder {
    this.salad.condiments.push(new Mayonnaise());
    this.salad.condimentsTotal[6]++;
    return this;
  }
  public getSalad(): Salad {
    return this.salad;
  }

}
